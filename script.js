const tabMenuItems = document.querySelectorAll('.tabs-title');
const tabContent = document.querySelectorAll('.content');

for(let i = 0; i < tabMenuItems.length; i++){
    tabMenuItems[i].addEventListener('click', (event) => {
        
        let tabCurrent = event.target.parentElement.children;
        for(let j = 0; j < tabCurrent.length; j++){
            tabCurrent[j].classList.remove('active')
        }
        event.target.classList.add('active');

        let contentCurrent = event.target.parentElement.nextElementSibling.children;
        for(let c = 0; c < contentCurrent.length; c++){
            contentCurrent[c].classList.remove('content-active')
        }

        tabContent[i].classList.add('content-active');       
        
    });
}